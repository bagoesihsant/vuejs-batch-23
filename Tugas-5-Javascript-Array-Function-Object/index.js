// Soal 01
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// mengurutkan kemudian tampilkan dalam array
var sortedDaftarHewan = daftarHewan.sort()
// menampilkan menggunakan loop
for (var i = 0; i < sortedDaftarHewan.length; i++) {
    console.log(sortedDaftarHewan[i])
}

// Soal 02
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

// membuat fungsi
function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + ", alamat saya di " +
        data.address + ", dan saya punya hobby yaitu " + data.hobby;
}

var perkenalan = introduce(data)
console.log(perkenalan)

// Soal 03
function hitung_huruf_vokal(string) {
    var arrString = string.toLowerCase().split('')
    var arrayVocal = ["a", "i", "u", "e", "o"];
    var jumlahVocal = 0;

    // mencari huruf vokal
    for (var i = 0; i < arrString.length; i++) {
        for (var j = 0; j < arrayVocal.length; j++) {
            if (arrString[i] == arrayVocal[j]) {
                jumlahVocal += 1;
            } else {
                jumlahVocal += 0;
            }
        }
    }

    return jumlahVocal;

}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2)

// Soal 4

function hitung(angka) {
    return (angka - 1) * 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));