// Soal 1
// buat function jumlah_kata, dengan input string
// function mengembalikan jumlah kata dalam kalimat tersebut
function jumlah_kata(string) {
    return string.split(" ").length;
}

// varibel
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";

// menampilkan hasil
console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))

// Soal 2
// buat functon next_date, dengan 3 parameter
// param: tanggal, bulan, tahun
// return: tanggal hari esok, dalam bentuk string
function next_date(tgl, bln, tahun) {
    var bulan = "";
    var kabisat = false;

    // Memeriksa apakah tahun kabisat atau tidak
    if (tahun % 4 == 0) {
        if (tahun % 100 == 0) {
            if (tahun % 400 == 0) {
                kabisat = true;
            } else {
                kabisat = false;
            }
        } else {
            kabisat = true;
        }
    } else {
        kabisat = false;
    }

    // Jika tahun kabisat
    if (kabisat == true) {
        if (bln == 2) {
            // memeriksa tanggal pada bulan februari
            if (tgl >= 29) {
                tgl = 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            } else {
                tgl += 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            }
        } else {
            // memeriksa tanggal pada bulan selain februari
            if (tgl >= 30) {
                tgl = 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            } else {
                tgl += 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            }
        }
    } else {
        // Jika tahun tidak kabisat
        if (bln == 2) {
            // memeriksa tanggal pada bulan februari
            if (tgl >= 28) {
                tgl = 1;// memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            } else {
                tgl += 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            }
        } else {
            // memeriksa tanggal pada bulan selain februari
            if (tgl >= 30) {
                tgl = 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            } else {
                tgl += 1;
                // memeriksa bulan 
                if (bln >= 12) {
                    bln = 1;
                    tahun += 1;
                } else {
                    bln += 1;
                }
            }
        }
    }

    // switch case bulan
    switch (bln) {
        case 1: { bulan = " Januari "; break; }
        case 2: { bulan = " Februari "; break; }
        case 3: { bulan = " Maret "; break; }
        case 4: { bulan = " April "; break; }
        case 5: { bulan = " Mei "; break; }
        case 6: { bulan = " Juni "; break; }
        case 7: { bulan = " Juli "; break; }
        case 8: { bulan = " Agustus "; break; }
        case 9: { bulan = " September "; break; }
        case 10: { bulan = " Oktober "; break; }
        case 11: { bulan = " November "; break; }
        case 12: { bulan = " Desember "; break; }
    }

    return tgl.toString().concat(bulan).concat(tahun);

}

// Contoh pertama
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));

// contoh kedua
var tanggal = 28;
var bulan = 2;
var tahun = 2021;
console.log(next_date(tanggal, bulan, tahun));

// contoh ketiga
var tanggal = 31;
var bulan = 12;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun))