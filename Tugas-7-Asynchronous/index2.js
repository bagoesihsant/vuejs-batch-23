// Soal 2
var readBooksPromise = require('./promise.js')

var booksPromise = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Buatlah kode untuk
// membaca buku buku dalam array booksPromise
// dengan input waktu adalah 10000ms (10 detik)
let readTimePromise = 10000;
// mencetak buku yang dibaca
const cetakTime = async () => {
    try {
        const sisaWaktuPertama = await readBooksPromise(readTimePromise, booksPromise[0]);
        const sisaWaktuKedua = await readBooksPromise(sisaWaktuPertama, booksPromise[1]);
        const sisaWaktuKetiga = await readBooksPromise(sisaWaktuKedua, booksPromise[2]);
        const sisaWaktuTerakhir = await readBooksPromise(sisaWaktuKetiga, booksPromise[0]);
    } catch (error) {
        if (error < 0) {
            console.log("Sisa Waktu: 0");
        }
    }
}

cetakTime()