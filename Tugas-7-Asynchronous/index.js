// Soal 1
var readBooks = require('./callback.js');

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
];

// Buatlah kode untuk
// membaca buku buku dalam array books
let readTime = 10000;

readBooks(readTime, books[3], (sisaWaktu) => {
    readBooks(sisaWaktu, books[1], (sisaWaktu) => {
        readBooks(sisaWaktu, books[0], (sisaWaktu) => {
            readBooks(sisaWaktu, books[2], (sisaWaktu) => {
                readBooks(sisaWaktu, books[1], (sisaWaktu) => { console.log("Waktu Tersisa: ".concat(sisaWaktu)); });
            });
        });
    });
});