// Soal 1
// buat arrow fuction untuk luas dan keliling persegi panjang menggunakan const dan let
let panjang = 10;
let lebar = 5;

const luas = (a, b) => {
    return a * b;
}

const keliling = (a, b) => {
    return (a * 2) + (b * 2);
}

// Mencetak nilai
console.log(luas(10, 5));
console.log(keliling(10, 5))

// Soal 2
// Ubah code berikut menjadi arrow function dan object literal es6 sederhana
const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName);
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName();

// Fungsi menggunakan es6
const newFunction6 = (firstName, lastName) => {
    return { firstName, lastName, fullName: () => { console.log(firstName + " " + lastName) } }
}

// Driver Code
newFunction6("Yusuf", "Akmal").fullName();

// Soal 3
// gunakan metode destructuring dalam es6 untuk mengambil semua nilai dalam object dengan lebih singkat
// hanya 1 line
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

// mengambil nilai dalam object
const { firstName, lastName, address, hobby } = newObject;

// menampilkan output
console.log(firstName, lastName, address, hobby);

// Soal 4
// kombinasikan kedua array menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// menggunakann ES5
const combined = west.concat(east);
// driver code
console.log(combined);

// Menggunakan ES6
const combined6 = [...west, ...east];
// driver code
console.log(combined6)

// Soal 5
// Sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6
const planet = "earth";
const view = "glass";
// mengggunakan es5
before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet;
// driver code
console.log(before);

// menggunakan es6
after5 = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
// driver code
console.log(after5);