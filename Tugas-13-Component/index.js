var cardMember = {
    template: `
        <div class="card h-100 w-100">
            <img :src="member.photo_profile ? photoDomain + member.photo_profile : 'https://dummyimage.com/600x400/efefef/030303'"
                alt="Foto Profile" class="card-img-top p-4">
            <div class="card-body">
                <h5 class="card-title">{{member.name}}</h5>
                <p class="card-text">Alamat: {{member.address}}</p>
                <p class="card-text">No. Telp: {{member.no_hp}}</p>
                <div class="row">
                    <div class="col-12 my-2">
                        <button @click="$emit('edit-member', member.id)" class="btn btn-primary w-100" >Edit
                            Member</button>
                    </div>
                    <div class="col-12 my-2">
                        <button @click="$emit('hapus-member', member.id)" class="btn btn-danger w-100" >Hapus
                            Member</button>
                    </div>
                    <div class="col-12 my-2">
                        <button @click="$emit('edit-foto', member.id)" class="btn btn-secondary w-100" >Edit
                            Foto</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <small class="text-muted">Last Update: {{member.updated_at}}</small>
            </div>
        </div>
    `,
    data: function () {
        return {
            photoDomain: "http://demo-api-vue.sanbercloud.com/"
        }
    },
    props: ['member'],
}


var vue = new Vue({
    el: "#content",
    components: {
        'component-card': cardMember
    },
    data: {
        nama: "",
        alamat: "",
        notelp: "",
        errors: {
            nama: "",
            alamat: "",
            notelp: ""
        },
        members: [],
        buttonStatus: "tambah",
        messageType: "",
        serverMessage: "",
        memberId: null
    },
    methods: {
        validateForm: function () {
            // Memeriksa apakah nama kosong atau tidak
            if (this.nama.length < 1) {
                Vue.set(this.errors, "nama", "Nama tidak boleh kosong")
                this.$refs.name.focus()
            } else {
                Vue.set(this.errors, "nama", "")
            }

            // Memeriksa apakah alamat kosong atau tidak
            if (this.alamat.length < 1) {
                Vue.set(this.errors, "alamat", "Alamat tidak boleh kosong")
                this.$refs.address.focus()
            } else if (this.alamat.length > 1 && this.alamat.length < 10) {
                Vue.set(this.errors, "alamat", "Alamat terlalu pendek. Alamat minimal 10 karakter.")
                this.$refs.address.focus()
            } else {
                Vue.set(this.errors, "alamat", "")
            }

            // memeriksa apakah nomor angka atau bukan
            if (parseInt(this.notelp) && parseFloat(this.notelp)) {
                // Jika notelp adalah angka
                // Memeriksa apakah no. telp kosong atau tidak
                if (this.notelp.length < 1) {
                    Vue.set(this.errors, "notelp", "No. Telp tidak boleh kosong")
                    this.$refs.no_hp.focus()
                } else if (this.notelp.length > 1 && this.notelp.length < 9) {
                    Vue.set(this.errors, "notelp", "No. Telp terlalu pendek")
                    this.$refs.no_hp.focus()
                } else {
                    Vue.set(this.errors, "notelp", "")
                }
            } else {
                // Jika notelp bukanlah angka
                Vue.set(this.errors, "notelp", "No. Telp harus berupa angka.")
                this.$refs.no_hp.focus()
            }
        },

        clearForm: function () {
            this.nama = ""
            this.alamat = ""
            this.notelp = ""
            this.buttonStatus = "tambah"
            this.memberId = null
        },

        tambahMember: function (event) {
            // Menghentikan halaman dari reload
            event.preventDefault()

            // Melakukan validasi terlebih dahulu
            this.validateForm()

            // memeriksa apakah terdapat error atau tidak
            if (this.errors.nama.length > 0 || this.errors.alamat.length > 0 || this.errors.notelp.length > 0) {
                // Jika terdapat error
                alert("Harap isi data dengan benar")
            } else {
                // Jika tidak terdapat error
                // Mulai menambahkan data

                // Mengambil seluruh data dari form
                let formData = new FormData()

                formData.append('name', this.nama)
                formData.append('address', this.alamat)
                formData.append('no_hp', this.notelp)

                // Membuat config
                let config = {
                    method: "post",
                    url: "http://demo-api-vue.sanbercloud.com/api/member",
                    data: formData
                }

                // Menjalankan ajax axios
                axios(config)
                    .then((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "success"
                        this.serverMessage = response.data.message
                    })
                    .catch((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "danger"
                        this.serverMessage = response.data.message
                    })
            }

        },

        editMember: function (id) {
            this.buttonStatus = "ubah"

            // mengambil data dari database
            let config = {
                method: "get",
                url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`
            }

            // menjalankan ajax menggunakan axios
            axios(config)
                .then((response) => {
                    this.nama = response.data.member.name
                    this.alamat = response.data.member.address
                    this.notelp = response.data.member.no_hp
                    this.memberId = response.data.member.id
                    this.$refs.name.focus()
                })
                .catch((response) => {
                    console.log(response)
                })

        },

        ubahMember: function (event, id) {
            // Menghentikan halaman dari reload
            event.preventDefault()

            // Melakukan validasi terlebih dahulu
            this.validateForm()

            // memeriksa apakah terdapat error atau tidak
            if (this.errors.nama.length > 0 || this.errors.alamat.length > 0 || this.errors.notelp.length > 0) {
                // Jika terdapat error
                alert("Harap isi data dengan benar")
            } else {
                // Jika tidak terdapat error
                // Mulai menambahkan data

                // Mengambil seluruh data dari form
                let formData = new FormData()

                formData.append('name', this.nama)
                formData.append('address', this.alamat)
                formData.append('no_hp', this.notelp)

                // Membuat config
                let config = {
                    method: "post",
                    url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`,
                    params: { _method: "PUT" },
                    data: formData
                }

                // Menjalankan ajax axios
                axios(config)
                    .then((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "success"
                        this.serverMessage = response.data.message
                    })
                    .catch((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "danger"
                        this.serverMessage = response.data.message
                    })
            }
        },

        hapusMember: function (id) {
            // membuat config
            let config = {
                method: "post",
                url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`,
                params: { _method: "DELETE" }
            }

            // menanyakan apakah yakin akan menghapus member atau tidak
            if (confirm("Apakah anda yakin akan menghapus member ?")) {
                // menjalankan ajax axios
                axios(config)
                    .then((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "success"
                        this.serverMessage = response.data.message
                    })
                    .catch((response) => {
                        this.clearForm()
                        this.ambilMember()
                        this.messageType = "danger"
                        this.serverMessage = response.data.message
                    })
            }


        },

        editFoto: function (id) {
            this.buttonStatus = "unggah"

            // mengambil data dari database
            let config = {
                method: "get",
                url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`
            }

            // menjalankan ajax menggunakan axios
            axios(config)
                .then((response) => {
                    this.nama = response.data.member.name
                    this.alamat = response.data.member.address
                    this.notelp = response.data.member.no_hp
                    this.memberId = response.data.member.id
                    this.$refs.photo_profile.focus()
                    console.log(response.data.member)
                })
                .catch((response) => {
                    console.log(response)
                })
        },

        unggahFoto: function (event, id) {
            // menghentikan event
            event.preventDefault()

            // mengambil data dari form
            let photoFile = this.$refs.photo_profile.files[0]

            let formData = new FormData()
            formData.append('photo_profile', photoFile)

            // membuat config
            let config = {
                method: 'post',
                url: `http://demo-api-vue.sanbercloud.com/api/member/${id}/upload-photo-profile`,
                data: formData
            }

            // menjalankan ajax axios
            axios(config)
                .then((response) => {
                    this.clearForm()
                    this.ambilMember()
                    this.messageType = "success"
                    this.serverMessage = response.data.message
                })
                .catch((response) => {
                    this.clearForm()
                    this.ambilMember()
                    this.messageType = "danger"
                    this.serverMessage = response.data.message
                })


        },

        batal: function (event) {
            // menghentikan reload halaman
            event.preventDefault()

            // melakukan reset DOM
            this.clearForm()
        },

        dismissAlert: function () {
            this.messageType = ""
            this.serverMessage = ""
        },

        ambilMember: function () {
            const config = {
                method: "get",
                url: "http://demo-api-vue.sanbercloud.com/api/member"
            }

            axios(config)
                .then((response) => {
                    this.members = response.data.members
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    },
    created() {
        this.ambilMember()
    }
});