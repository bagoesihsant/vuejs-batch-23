// Soal Nomor 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// gabungkan kedua variabel tersebut, sehingga hasil adalah "saya senang belajar JAVASCRIPT"
// Memulai pemecahan String
var first_word = pertama.substr(0, 5);
var second_word = pertama.substr(pertama.indexOf('senang'), 7);
var third_word = kedua.substr(0, 8);
var special_word = kedua.substr(kedua.indexOf('javascript'), 10);
// menghasilkan output
console.log(first_word.concat(second_word).concat(third_word).concat(special_word.toUpperCase()));

// Soal Nomor 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// ubahlah variabel diatas menjadi integer, dan lakukan operasi matematika agar variabel
// menghasilkan output 24 (integer)
var sum_var = parseInt(kataPertama) + parseInt(kataKedua) * parseInt(kataKetiga) + parseInt(kataKeempat);
var sum_var_2 = (parseInt(kataPertama) - parseInt(kataKetiga) + parseInt(kataKeempat)) * parseInt(kataKedua);
var sum_var_3 = (parseInt(kataPertama) - parseInt(kataKetiga) - parseInt(kataKedua)) * parseInt(kataKeempat);
var sum_var_4 = (parseInt(kataPertama) - parseInt(kataKeempat) + parseInt(kataKedua)) * parseInt(kataKetiga);
console.log(sum_var);
console.log(sum_var_2);
console.log(sum_var_3);
console.log(sum_var_4);

// Soal Nomor 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(kalimat.indexOf('javascript'), kalimat.indexOf('itu'));
var kataKetiga = kalimat.substring(kalimat.indexOf('itu'), kalimat.indexOf('keren'));
var kataKeempat = kalimat.substring(kalimat.indexOf('keren'), kalimat.indexOf('sekali'));
var kataKelima = kalimat.substring(kalimat.indexOf('sekali'), kalimat.length);

console.log('Kata Pertama: '.concat(kataPertama));
console.log('Kata Kedua: '.concat(kataKedua));
console.log('Kata Ketiga: '.concat(kataKetiga));
console.log('Kata Keempat: '.concat(kataKeempat));
console.log('Kata Kelima: '.concat(kataKelima));