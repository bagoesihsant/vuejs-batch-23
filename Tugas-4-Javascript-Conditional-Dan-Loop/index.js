// Soal 1
var nilai = 98;

// Pengkondisian
if (nilai >= 0 && nilai <= 100) {
    if (nilai >= 85) {
        console.log("A");
    } else if (nilai >= 75 && nilai < 85) {
        console.log("B");
    } else if (nilai >= 65 && nilai < 75) {
        console.log("C");
    } else if (nilai >= 55 && nilai < 65) {
        console.log("D")
    } else {
        console.log("E")
    }
} else {
    console.log("Nilai anda melebihi kapasitas yang ditentukan!")
}

// Soal 2
var tanggal = 17;
var bulan = 7;
var tahun = 2000;

// switch case
switch (bulan) {
    case 1: { console.log(tanggal.toString().concat(' Januari ').concat(tahun.toString())); break; }
    case 2: { console.log(tanggal.toString().concat(' Februari ').concat(tahun.toString())); break; }
    case 3: { console.log(tanggal.toString().concat(' Maret ').concat(tahun.toString())); break; }
    case 4: { console.log(tanggal.toString().concat(' April ').concat(tahun.toString())); break; }
    case 5: { console.log(tanggal.toString().concat(' Mei ').concat(tahun.toString())); break; }
    case 6: { console.log(tanggal.toString().concat(' Juni ').concat(tahun.toString())); break; }
    case 7: { console.log(tanggal.toString().concat(' Juli ').concat(tahun.toString())); break; }
    case 8: { console.log(tanggal.toString().concat(' Agustus ').concat(tahun.toString())); break; }
    case 9: { console.log(tanggal.toString().concat(' September ').concat(tahun.toString())); break; }
    case 10: { console.log(tanggal.toString().concat(' Oktober ').concat(tahun.toString())); break; }
    case 11: { console.log(tanggal.toString().concat(' November ').concat(tahun.toString())); break; }
    case 12: { console.log(tanggal.toString().concat(' Desember ').concat(tahun.toString())); break; }
}

// Soal 3
n = 7
for (var i = 0; i <= n; i++) {
    var baris = ''
    for (var j = 0; j < i; j++) {
        baris += '#'
    }
    console.log(baris)
}

// Soal 4
var m = 10;
var counter = 1;
for (var i = 1; i <= m; i++) {
    if (counter <= 3) {
        switch (counter) {
            case 1: { console.log(i.toString().concat(" - I love programming")); counter++; break; }
            case 2: { console.log(i.toString().concat(" - I love Javascript")); counter++; break; }
            case 3: { console.log(i.toString().concat(" - I love VueJS")); console.log("==="); counter = 1; break; }
        }
    } else {
        counter = 1;
    }
}